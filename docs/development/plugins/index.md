# Plugins

TFA plugins provide the form and validation handling for 2nd factor
authentication of a user. The TFA module will interrupt a username
and password authentication and begin the TFA process (see Configuration for
exceptions to this statement), passing off the form control and validation to
the active plugin.
